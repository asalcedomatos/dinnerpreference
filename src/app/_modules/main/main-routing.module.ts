import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PreferenceComponent} from '../../preference/preference.component';
import { PreferenceListComponent } from '../../preference-list/preference-list.component';
import { MasterComponent} from '../../master/master.component';
const routes: Routes = [
  {
    path:'',
    redirectTo : 'main',
    pathMatch : 'full'
  },
  {
    path:'main',
    component:MasterComponent,
    children:[
      {
        path:'',
        redirectTo:'preference',
        pathMatch:'full'
      },
      {
        path:'preference',
        component:PreferenceComponent
      },
      {
        path:'list',
        component: PreferenceListComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
