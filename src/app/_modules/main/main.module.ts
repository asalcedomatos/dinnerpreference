import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterComponent} from '../../master/master.component';
import { PreferenceComponent } from '../../preference/preference.component'
import { MainRoutingModule } from './main-routing.module';
import { MaterialModule } from '../../material.module';
import { PreferenceListComponent } from '../../preference-list/preference-list.component';

@NgModule({
  declarations: [MasterComponent,PreferenceComponent, PreferenceListComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    MaterialModule
  ],
  entryComponents: [MasterComponent],
  bootstrap: [MasterComponent]
})
export class MainModule { }
