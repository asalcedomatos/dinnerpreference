import { Component, OnInit } from '@angular/core';
import { Persons } from '../_interface/person';
import { environment } from '../../environments/environment';
import swal from 'sweetalert2';

@Component({
  selector: 'app-preference',
  templateUrl: './preference.component.html',
  styleUrls: ['./preference.component.css']
})
export class PreferenceComponent implements OnInit {
  public per: Persons = { name: '', dinner: '', position: 0 };
  public emv = environment;
  constructor() {
  }

  setChair(value) {
    this.per.position = value;
  }
  setPerson() {
    this.per.name !== "" && this.per.name !== " " && this.per.dinner !== "" && this.per.dinner !== " " && this.per.position > 0 ?
      swal.fire({
        title: 'Confirm Saving',
        text: `¿Are you sure you want to save this preferences? \n${this.per.name + ' ' + this.per.dinner + ' ' + this.per.position}?`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ok',
        cancelButtonText: 'Cancel'
      }).then((result) => {
        if (result.value) {
          this.emv.regPersons.push(this.per);
          this.resetPerson();
          swal.fire('Success', 'Preference saved successfully', 'success')
        }
      }) :
      swal.fire({
        title: 'Ops..!',
        text: 'All imputs are required, please Verify...',
        type: 'warning'
      });
  }
  resetPerson(){
    this.per = { name: '', dinner: '', position: 0 };
  }
  ngOnInit() {
  }

}
